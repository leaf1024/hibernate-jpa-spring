package com.demo.dao;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.demo.entity.User;

@Repository
public class UserDao {

	@Autowired
	private HibernateDao hibernateDao;
	
	@Transactional
	public void save(User user){
		Session session=hibernateDao.getSession();
        session.save(user);  
        throw new RuntimeException();
	}
}
